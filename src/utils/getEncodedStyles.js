const getEncodedStyles = (styles) => {
  let ret = '';
  let styleparse_types = {
    all: '0',
    administrative: '1',
    'administrative.country': '17',
    'administrative.land_parcel': '21',
    'administrative.locality': '19',
    'administrative.neighborhood': '20',
    'administrative.province': '18',
    landscape: '5',
    'landscape.man_made': '81',
    'landscape.natural': '82',
    poi: '2',
    'poi.attraction': '37',
    'poi.business': '33',
    'poi.government': '34',
    'poi.medical': '36',
    'poi.park': '40',
    'poi.place_of_worship': '38',
    'poi.school': '35',
    'poi.sports_complex': '39',
    road: '3',
    'road.arterial': '50',
    'road.highway': '49',
    'road.local': '51',
    transit: '4',
    'transit.line': '65',
    'transit.station': '66',
    water: '6',
  };
  let styleparse_elements = {
    all: 'a',
    geometry: 'g',
    'geometry.fill': 'g.f',
    'geometry.stroke': 'g.s',
    labels: 'l',
    'labels.icon': 'l.i',
    'labels.text': 'l.t',
    'labels.text.fill': 'l.t.f',
    'labels.text.stroke': 'l.t.s',
  };
  let styleparse_stylers = {
    color: 'p.c',
    gamma: 'p.g',
    hue: 'p.h',
    invert_lightness: 'p.il',
    lightness: 'p.l',
    saturation: 'p.s',
    visibility: 'p.v',
    weight: 'p.w',
  };
  for (let i = 0; i < styles.length; i++) {
    if (styles[i].featureType) {
      ret += 's.t:' + styleparse_types[styles[i].featureType] + '|';
    }
    if (styles[i].elementType) {
      if (!styleparse_elements[styles[i].elementType])
        console.log('style element transcription unkown:' + styles[i].elementType);
      ret += 's.e:' + styleparse_elements[styles[i].elementType] + '|';
    }
    if (styles[i].stylers) {
      for (let u = 0; u < styles[i].stylers.length; u++) {
        let cstyler = styles[i].stylers[u];
        for (let k in cstyler) {
          if (k === 'color') {
            if (cstyler[k].length === 7) cstyler[k] = '#ff' + cstyler[k].slice(1);
            else if (cstyler[k].length !== 9) console.log('malformed color:' + cstyler[k]);
          }
          ret += styleparse_stylers[k] + ':' + cstyler[k] + '|';
        }
      }
    }
    ret = ret.slice(0, ret.length - 1);
    ret += ',';
  }
  return encodeURIComponent(ret.slice(0, ret.length - 1));
};

export default getEncodedStyles;
