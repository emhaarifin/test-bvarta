const { SHOW_LEFT_SIDEBAR, SHOW_RIGHT_SIDEBAR } = require('./constants');

const initialState = {
  showLeftSidebar: false,
  showRightSideBar: false,
};

const sidebarReducer = (state = initialState, action) => {
  switch (action.type) {
    case SHOW_LEFT_SIDEBAR:
      return {
        ...state,
        showLeftSidebar: !state.showLeftSidebar,
      };
    case SHOW_RIGHT_SIDEBAR:
      return {
        ...state,
        showRightSideBar: !state.showRightSideBar,
      };
    default:
      return state;
  }
};

export default sidebarReducer;
