const { SHOW_LEFT_SIDEBAR, SHOW_RIGHT_SIDEBAR } = require('./constants');

export const showLeftSidebar = () => ({
  type: SHOW_LEFT_SIDEBAR,
});

export const showRightSideBar = () => ({
  type: SHOW_RIGHT_SIDEBAR,
});
