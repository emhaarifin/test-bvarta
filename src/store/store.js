import { createStore, combineReducers, compose } from 'redux';

import { persistStore, persistReducer } from 'redux-persist';
import { composeWithDevTools } from 'redux-devtools-extension';
import sidebarReducer from './sidebar/reducer';
import storage from 'redux-persist/lib/storage';
import themeReducer from './theme/reducer';
import localizationReducer from './localization/reducers';
import mapReducer from './map/reducer';
const persistConfig = {
  key: 'root',
  storage,
  whitelist: ['theme', 'localization'],
};

const rootReducers = combineReducers({
  sidebar: sidebarReducer,
  theme: themeReducer,
  localization: localizationReducer,
  map: mapReducer,
});

const isProduction = process.env.NODE_ENV === 'production';

const composeEnhancer = isProduction ? compose() : composeWithDevTools();

const persistedReducer = persistReducer(persistConfig, rootReducers);
export const store = createStore(persistedReducer, composeEnhancer);
export const persistor = persistStore(store);
