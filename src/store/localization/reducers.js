const { SET_LOCALIZATION } = require('./constants');

const initialState = {
  language: 'id',
  region: 'id-ID',
};

const localizationReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_LOCALIZATION:
      return {
        ...state,
        language: action.payload.language,
        region: action.payload.region,
      };

    default:
      return state;
  }
};

export default localizationReducer;
