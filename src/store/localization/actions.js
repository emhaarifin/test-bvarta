import { SET_LOCALIZATION } from './constants';

export const handleChangeLocale = ({ language, region }) => ({
  type: SET_LOCALIZATION,
  payload: { language, region },
});
