import { CHANGE_MODE } from './constants';

export const changeMode = () => ({
  type: CHANGE_MODE,
});
