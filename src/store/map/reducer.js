const initialState = {
  location: [
    {
      region: 'Jakarta, Indonesia',
      lat: -6.2,
      lng: 106.816666,
    },
    {
      region: 'Pamekasan, Pamekasan Regency, East Jawa, Indonesia',
      lat: -7.161367,
      lng: 113.482498,
    },
    {
      region: 'Banda Aceh, Banda Aceh City, Aceh, Indonesia',
      lat: 5.54829,
      lng: 95.323753,
    },
    {
      region: 'Ambon, Ambon Island, Maluku, Indonesia',
      lat: -3.654703,
      lng: 128.190643,
    },
    {
      region: 'Palopo, Wara, South Sulawesi, Indonesia',
      lat: -2.994494,
      lng: 120.195465,
    },
    {
      region: 'Soreang, Bandung, West Java, Indonesia',
      lat: -7.025253,
      lng: 107.51976,
    },
    {
      region: 'Dumai, Dumai Province, Riau, Indonesia',
      lat: 1.694394,
      lng: 101.445007,
    },
    {
      region: 'Pematang Siantar City, North Sumatra, Indonesia',
      lat: 2.970042,
      lng: 99.068169,
    },
    {
      region: 'Banjarsari, Surakarta City, Central Java, Indonesia',
      lat: -7.550676,
      lng: 110.828316,
    },
    {
      region: 'Lhoksukon, Aceh Utara, Aceh, Indonesia',
      lat: 5.0517009999999996,
      lng: 97.318123,
    },
  ],
};

const mapReducer = (state = initialState, action) => {
  switch (action.type) {
    default:
      return state;
  }
};

export default mapReducer;
