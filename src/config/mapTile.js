const mapTile = {
  google: ({ lyrs, hl, x, y, z, s, apistyle, key }) => ({
    url: 'https://mt0.google.com/vt/',
    queryParams: { lyrs, hl, x, y, z, s, apistyle, key },
  }),
};

export default mapTile;
