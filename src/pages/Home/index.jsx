import React, { lazy, useEffect, useState } from 'react';
import Default from 'layouts/default';
import { useSelector } from 'react-redux';
import { darkMode } from 'constants/mapStyle';
import { MapContainer, Marker, TileLayer } from 'react-leaflet';
import getEncodedStyles from 'utils/getEncodedStyles';
import mapTile from 'config/mapTile';
const SidebarLeft = lazy(() => import('components/Sidebar/left'));
const SidebarRight = lazy(() => import('components/Sidebar/right'));
const HomePage = () => {
  const [marker, setMarker] = useState(null);
  const [map, setMap] = useState();
  const { language } = useSelector((state) => state.localization);
  const [mount, setMount] = useState(true);
  const theme = useSelector((state) => state.theme);
  // re mount for detect change language
  useEffect(() => {
    setMount((prev) => !prev);
  }, [language, theme]);
  useEffect(() => {
    if (!mount) {
      setMount(true);
    }
  }, [mount]);

  const handlerMarker = ({ lat, lng }) => {
    setMarker({ lat, lng });
    map.flyTo({ lat, lng }, 13);
  };

  useEffect(() => {
    if (map) {
      const fixMap = setInterval(() => {
        map.invalidateSize();
      }, 0);

      return () => clearInterval(fixMap);
    }
  }, [map]);

  const resMapTile = () => {
    let i = 0;
    const { url, queryParams } = mapTile.google({
      lyrs: 'm',
      hl: language,
      x: '{x}',
      y: '{y}',
      z: '{z}',
      s: 'Ga',
      apistyle: getEncodedStyles(darkMode),
      key: process.env.REACT_APP_GOOGLE_API_KEY ?? '',
    });
    if (theme.mode === 'light') delete queryParams.apistyle;

    let urlQueryParams = '';
    for (const key in queryParams) {
      if (queryParams.hasOwnProperty.call(queryParams, key)) {
        urlQueryParams += (i > 0 ? '&' : '') + key + '=' + queryParams[key];
        i++;
      }
    }

    return url + urlQueryParams;
  };

  return (
    <Default>
      <SidebarLeft setMarker={handlerMarker} />
      <div className='flex width--100' style={{ height: '100%' }}>
        <MapContainer
          center={{ lat: 0, lng: 0 }}
          zoom={3}
          id='map'
          zoomControl={false}
          whenCreated={(map) => {
            setMap(map);
          }}
        >
          {mount && (
            <TileLayer
              url={resMapTile()}
              // url={`https://maps.wikimedia.org/osm-intl/{z}/{x}/{y}.png?lang=${language}`}

              attribution='Map data &copy; <a href="http://openstreetmap.org/copyright">OpenStreetMap contributors</a>'
            />
          )}
          {Boolean(marker) && <Marker position={{ lat: marker.lat, lng: marker.lng }} />}
        </MapContainer>
      </div>
      <SidebarRight />
    </Default>
  );
};

export default HomePage;
