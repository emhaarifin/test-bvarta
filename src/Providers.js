import React from 'react';
import { Provider as ReduxProvider } from 'react-redux';
import { store, persistor } from 'store/store';
import { PersistGate } from 'redux-persist/integration/react';

const Providers = ({ children }) => {
  return (
    <ReduxProvider store={store}>
      <PersistGate persistor={persistor} loading={<div />}>
        {children}
      </PersistGate>
    </ReduxProvider>
  );
};

export default Providers;
