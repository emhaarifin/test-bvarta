import { useMediaQuery } from 'hooks/useMediaQuery';
import React, { Fragment, lazy } from 'react';
import { useSelector } from 'react-redux';
import MobileNav from './mobileNav';
import styles from './styles.module.scss';
const Header = lazy(() => import('./header'));
const Default = ({ children }) => {
  const theme = useSelector((state) => state.theme);

  const isTablet = useMediaQuery(992);

  return (
    <Fragment>
      <div data-theme={theme.mode} style={{ width: '100%', height: '100%' }}>
        <Header></Header>
        <main className={styles.main}>{children}</main>
        {isTablet && <MobileNav />}
      </div>
    </Fragment>
  );
};

export default Default;
