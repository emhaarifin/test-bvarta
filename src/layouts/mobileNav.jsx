import Hamburger from 'components/Hamburger';
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { showLeftSidebar, showRightSideBar } from 'store/sidebar/actions';

import styles from './styles.module.scss';
const MobileNav = () => {
  const dispatch = useDispatch();

  const handleLeftSidebar = () => dispatch(showLeftSidebar());
  const handleRightSidebar = () => dispatch(showRightSideBar());

  const sidebar = useSelector((state) => state.sidebar);
  return (
    <nav className={styles.mobileNav}>
      <div className='flex flex--justify-space-between px-3 flex--align-center '>
        <Hamburger onClick={handleLeftSidebar} open={sidebar.showLeftSidebar} />
        <Hamburger onClick={handleRightSidebar} open={sidebar.showRightSideBar} />
      </div>
    </nav>
  );
};

export default MobileNav;
