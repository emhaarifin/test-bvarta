import ToogleLocalization from 'components/ToogleLocalization';
import ToogleTheme from 'components/ToogleTheme';
import images from 'constants/images';
import React from 'react';

const Header = () => {
  return (
    <header className='px-2 py-4 flex flex--justify-space-between flex--align-end'>
      <section className='width--70'>
        <div className='flex flex--justify-space-between flex--align-center '>
          <ToogleTheme />
          <ToogleLocalization />
        </div>
      </section>
      <section className='width--30'>
        <div className='flex flex--justify-end'>
          <img
            src={images.Arifin}
            alt='Muhammad Arifin'
            width={100}
            height={100}
            style={{ objectFit: 'cover', borderRadius: '50%' }}
          ></img>
        </div>
      </section>
    </header>
  );
};

export default Header;
