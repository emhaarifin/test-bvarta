import React from 'react';
import cn from 'utils/cn';
import styles from './styles.module.scss';
const Button = ({ className, children, ...props }) => {
  return (
    <button className={cn(`${styles.button} ${className}`)} {...props}>
      {children}
    </button>
  );
};

export default Button;
