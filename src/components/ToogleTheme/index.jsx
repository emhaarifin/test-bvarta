import Button from 'components/Button';
import Moon from 'components/Icons/moon';
import Sun from 'components/Icons/sun';
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { changeMode } from 'store/theme/actions';
import styles from './styles.module.scss';
const ToogleTheme = () => {
  const theme = useSelector((state) => state.theme);
  const dispatch = useDispatch();
  const changeTheme = () => dispatch(changeMode());
  return (
    <Button onClick={changeTheme} className={styles.toogleTheme} aria-label='toogle-theme'>
      {theme.mode === 'dark' ? <Moon fill={theme.mode === 'dark' && 'white'} /> : <Sun />}
    </Button>
  );
};

export default ToogleTheme;
