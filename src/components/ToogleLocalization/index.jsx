import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { handleChangeLocale } from 'store/localization/actions';

const languageList = [
  {
    name: 'Bahasa Indonesia',
    language: 'id',
    region: 'id-ID',
  },
  {
    name: 'English',
    language: 'en',
    region: 'en-US',
  },
];

const ToogleLocalization = () => {
  const dispatch = useDispatch();
  const localization = useSelector((state) => state.localization);
  const handleChange = async (e) => {
    const { language, region } = JSON.parse(e.target.value);
    await dispatch(handleChangeLocale({ language, region }));
  };

  return (
    <div>
      <select onChange={handleChange} value={JSON.stringify({ ...localization })}>
        {languageList.map((language) => (
          <option key={language.name} value={JSON.stringify({ language: language.language, region: language.region })}>
            {language.name}
          </option>
        ))}
      </select>
    </div>
  );
};

export default ToogleLocalization;
