import React, { Fragment } from 'react';
import { useSelector } from 'react-redux';
import Sidebar from '../index';
import Button from 'components/Button';
import styles from './styles.module.scss';
import cn from 'utils/cn';
const SidebarLeft = ({ setMarker = () => {} }) => {
  const mapState = useSelector((state) => state.map);
  const { showLeftSidebar } = useSelector((state) => state.sidebar);
  return (
    <Fragment>
      <Sidebar className={cn(`${styles.sidebarLeft} ${showLeftSidebar ? styles.sidebarLeftOpen : ''}`)}>
        {mapState.location.map((l) => (
          <Button
            key={l.region}
            onClick={() => setMarker({ lat: l.lat, lng: l.lng })}
            className='width--100 background--cyan800 py-2 my-1 text--white'
          >
            <span className='text--max-1-line width-100'> {l.region}</span>
          </Button>
        ))}
      </Sidebar>
    </Fragment>
  );
};

export default SidebarLeft;
