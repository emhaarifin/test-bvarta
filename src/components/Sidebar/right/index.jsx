import React from 'react';
import Sidebar from '../index';
import styles from './styles.module.scss';
import { useSelector } from 'react-redux';
import cn from 'utils/cn';
const SidebarRight = () => {
  const { showRightSideBar } = useSelector((state) => state.sidebar);
  return (
    <Sidebar className={cn(`${styles.sidebarRight} ${showRightSideBar ? styles.sidebarRightOpen : ''}`)}></Sidebar>
  );
};

export default SidebarRight;
