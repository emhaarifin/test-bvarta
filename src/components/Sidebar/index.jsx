import React from 'react';
import cn from 'utils/cn';
import styles from './styles.module.scss';
const Sidebar = ({ children, className = "", ...props }) => {
  return (
    <div className={cn(`${styles.sidebar} ${className}`)} {...props}>
      {children}
    </div>
  );
};

export default Sidebar;
