import React from 'react';
import cn from 'utils/cn';
import styles from './styles.module.scss';
const Hamburger = ({ open, onClick }) => {
  return (
    <div className={styles.container} onClick={onClick}>
      <div className={styles.lineContainer}>
        <div className={styles.inner}>
          <div className={cn(`${styles.line} ${open ? styles.lineOpen : ''}`)} />
          <div className={cn(`${styles.line} ${open ? styles.lineOpen : ''}`)} />
          <div className={cn(`${styles.line} ${open ? styles.lineOpen : ''}`)} />
        </div>
      </div>
    </div>
  );
};

export default Hamburger;
