import Providers from 'Providers';
import { lazy, Suspense } from 'react';
const HomePage = lazy(() => import('./pages/Home'));
function App() {
  return (
    <Suspense fallback={<></>}>
      <Providers>
        <HomePage />
      </Providers>
    </Suspense>
  );
}

export default App;
